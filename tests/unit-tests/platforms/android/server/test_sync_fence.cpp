/*
 * Copyright © 2013 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Kevin DuBois <kevin.dubois@canonical.com>
 */

#include "sync_fence.h"
#include "mir/test/doubles/mock_fence.h"

#include <linux/sync.h>
#include <sys/ioctl.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>

namespace mga = mir::graphics::android;
namespace mtd = mir::test::doubles;

namespace
{
struct MockFileOps : public mga::SyncFileOps
{
    MOCK_METHOD2(sync_wait, int(int,int));
    MOCK_METHOD3(sync_merge, int(const char*,int,int));
    MOCK_METHOD1(dup, int(int));
    MOCK_METHOD1(close, int(int));
};
}

class SyncSwTest : public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        mock_fops = std::make_shared<testing::NiceMock<MockFileOps>>();
        dummy_fd_value = 3;
    }

    int dummy_fd_value{3};
    int invalid_fd_value{-1};
    mir::Fd dummy_fd{std::move(dummy_fd_value)};
    mir::Fd invalid_fd{std::move(invalid_fd_value)};
    std::shared_ptr<MockFileOps> mock_fops;
};

TEST_F(SyncSwTest, sync_wait)
{
    EXPECT_CALL(*mock_fops, sync_wait(dummy_fd_value, -1))
        .Times(1);
    mga::SyncFence fence1(mock_fops, std::move(dummy_fd));
    fence1.wait();

    //will not call ioctl
    mga::SyncFence fence2(mock_fops, std::move(invalid_fd));
    fence2.wait();
}

//timeout is in msecs
TEST_F(SyncSwTest, sync_wait_with_timeout_times_out)
{
    using namespace std::literals::chrono_literals;
    auto timeout = 150ms;
    EXPECT_CALL(*mock_fops, sync_wait(dummy_fd_value, timeout.count()))
        .WillOnce(testing::Return(-1));

    mga::SyncFence fence1(mock_fops, std::move(dummy_fd));
    EXPECT_FALSE(fence1.wait_for(timeout));
}

TEST_F(SyncSwTest, sync_wait_with_timeout_clears)
{
    using namespace std::literals::chrono_literals;
    auto timeout = 150ms;
    EXPECT_CALL(*mock_fops, sync_wait(dummy_fd_value, timeout.count()))
        .WillOnce(testing::Return(0));

    mga::SyncFence fence1(mock_fops, std::move(dummy_fd));
    EXPECT_TRUE(fence1.wait_for(timeout));
}

TEST_F(SyncSwTest, sync_merge_with_valid_fd)
{
    using namespace testing;
    int dummy_fd2 = 44;
    EXPECT_CALL(*mock_fops, sync_merge(_, dummy_fd_value, dummy_fd2))
        .Times(1);

    mga::SyncFence fence1(mock_fops, std::move(dummy_fd));
    fence1.merge_with(dummy_fd2);
}

TEST_F(SyncSwTest, sync_merge_with_invalid_fd)
{
    using namespace testing;
    EXPECT_CALL(*mock_fops, sync_merge(_, dummy_fd_value, _))
        .Times(0);

    mga::SyncFence fence1(mock_fops, std::move(dummy_fd));
    fence1.merge_with(invalid_fd_value);
}

TEST_F(SyncSwTest, copy_dups_fd)
{
    using namespace testing;
    int fd2 = dummy_fd_value + 1;
    EXPECT_CALL(*mock_fops, dup(dummy_fd_value))
        .Times(1)
        .WillOnce(Return(fd2));;

    mga::SyncFence fence(mock_fops, std::move(dummy_fd));

    EXPECT_EQ(fd2, fence.copy_native_handle());
}
